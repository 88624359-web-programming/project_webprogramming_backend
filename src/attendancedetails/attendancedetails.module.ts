import { Module } from '@nestjs/common';
import { AttendancedetailsService } from './attendancedetails.service';
import { AttendancedetailsController } from './attendancedetails.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AttendanceDetail } from './entities/attendancedetail.entity';

@Module({
  imports: [TypeOrmModule.forFeature([AttendanceDetail])],
  controllers: [AttendancedetailsController],
  providers: [AttendancedetailsService],
})
export class AttendancedetailsModule {}
