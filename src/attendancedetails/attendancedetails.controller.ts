import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { AttendancedetailsService } from './attendancedetails.service';
import { CreateAttendancedetailDto } from './dto/create-attendancedetail.dto';
import { UpdateAttendancedetailDto } from './dto/update-attendancedetail.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

//@UseGuards(JwtAuthGuard)
@Controller('attendancedetails')
export class AttendancedetailsController {
  constructor(
    private readonly attendancedetailsService: AttendancedetailsService,
  ) {}

  @Post()
  create(@Body() createAttendancedetailDto: CreateAttendancedetailDto) {
    return this.attendancedetailsService.create(createAttendancedetailDto);
  }

  @Get()
  findAll() {
    return this.attendancedetailsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.attendancedetailsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateAttendancedetailDto: UpdateAttendancedetailDto,
  ) {
    return this.attendancedetailsService.update(+id, updateAttendancedetailDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.attendancedetailsService.remove(+id);
  }
}
