import { Test, TestingModule } from '@nestjs/testing';
import { AttendancedetailsService } from './attendancedetails.service';

describe('AttendancedetailsService', () => {
  let service: AttendancedetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AttendancedetailsService],
    }).compile();

    service = module.get<AttendancedetailsService>(AttendancedetailsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
