import { PartialType } from '@nestjs/mapped-types';
import { CreateAttendancedetailDto } from './create-attendancedetail.dto';

export class UpdateAttendancedetailDto extends PartialType(
  CreateAttendancedetailDto,
) {}
