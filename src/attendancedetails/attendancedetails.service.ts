import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateAttendancedetailDto } from './dto/create-attendancedetail.dto';
import { UpdateAttendancedetailDto } from './dto/update-attendancedetail.dto';
import { AttendanceDetail } from './entities/attendancedetail.entity';

@Injectable()
export class AttendancedetailsService {
  constructor(
    @InjectRepository(AttendanceDetail)
    private attendanceDetailsRepository: Repository<AttendanceDetail>,
  ) {}

  create(createAttendancedetailDto: CreateAttendancedetailDto) {
    return this.attendanceDetailsRepository.save(createAttendancedetailDto);
  }

  findAll() {
    return this.attendanceDetailsRepository.find({
      relations: {
        employee: true,
      },
    });
  }

  findOne(id: number) {
    return this.attendanceDetailsRepository.findOne({
      where: { id },
      relations: {
        employee: true,
      },
    });
  }

  async update(
    id: number,
    updateAttendancedetailDto: UpdateAttendancedetailDto,
  ) {
    const ref = await this.attendanceDetailsRepository.findOneBy({ id });
    if (!ref) {
      throw new NotFoundException();
    }
    const updated = { ...ref, ...updateAttendancedetailDto };
    return this.attendanceDetailsRepository.save(updated);
  }

  async remove(id: number) {
    const ref = await this.attendanceDetailsRepository.findOneBy({ id });
    if (!ref) {
      throw new NotFoundException();
    }
    return this.attendanceDetailsRepository.softRemove(ref);
  }
}
