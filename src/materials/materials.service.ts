import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateMaterialDto } from './dto/create-material.dto';
import { UpdateMaterialDto } from './dto/update-material.dto';
import { Material } from './entities/material.entity';

@Injectable()
export class MaterialsService {
  constructor(
    @InjectRepository(Material)
    private materialsRepository: Repository<Material>,
  ) {}

  async create(createMaterialDto: CreateMaterialDto) {
    const { name } = createMaterialDto;
    const materialExists = await this.nameExists(name);
    if (materialExists) {
      throw new ConflictException('Material with this name already exists');
    }
    const material = this.materialsRepository.create(createMaterialDto);
    return this.materialsRepository.save(material);
  }

  findAll() {
    return this.materialsRepository.find();
  }

  findOne(id: number) {
    return this.materialsRepository.findOne({ where: { id } });
  }

  async findOneByName(name: string) {
    return this.materialsRepository.findOne({
      where: { name: name },
    });
  }

  async nameExists(name: string): Promise<boolean> {
    const material = await this.materialsRepository.findOne({
      where: { name },
    });
    return !!material;
  }

  async update(id: number, updateMaterialDto: UpdateMaterialDto) {
    const refMaterial = await this.materialsRepository.findOneBy({ id });
    if (!refMaterial) {
      throw new NotFoundException();
    }
    const updatedMaterial = { ...refMaterial, ...updateMaterialDto };
    return this.materialsRepository.save(updatedMaterial);
  }

  async remove(id: number) {
    const refMaterial = await this.materialsRepository.findOneBy({ id });
    if (!refMaterial) {
      throw new NotFoundException();
    }
    return this.materialsRepository.softRemove(refMaterial);
  }
}
