import { IsInt, IsNotEmpty, Matches, Min } from 'class-validator';

export class CreateMaterialDto {
  @IsNotEmpty()
  @Matches(/^[\u0E00-\u0E7F -~]{2,32}$/, {
    message:
      'Material category must be between 2 and 32 characters long and can contain Thai or English alphabet, special characters, or numbers.',
  })
  category: string;

  @IsNotEmpty()
  @Matches(/^[\u0E00-\u0E7F -~]{2,64}$/, {
    message:
      'Material name must be between 2 and 64 characters long and can contain Thai or English alphabet, special characters, or numbers.',
  })
  name: string;

  @IsNotEmpty()
  @IsInt({ message: 'Minimum values must be an integer.' })
  @Min(0, { message: 'Minimum values cant be less than "0".' })
  minimum: number;

  @IsNotEmpty()
  @IsInt({ message: 'On hand values must be an integer.' })
  @Min(0, { message: 'On hand values cant be less than "0".' })
  onhand: number;

  @IsNotEmpty()
  @Matches(/^[\u0E00-\u0E7F -~]{2,32}$/, {
    message:
      'Material unit must be between 2 and 32 characters long and can contain Thai or English alphabet, special characters, or numbers.',
  })
  unit: string;
}
