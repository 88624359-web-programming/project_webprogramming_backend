import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Employee } from 'src/employees/entities/employee.entity';
import { EmployeesService } from '../employees/employees.service';
import * as bcrypt from 'bcrypt';
import { log } from 'console';

@Injectable()
export class AuthService {
  constructor(
    private employeesService: EmployeesService,
    private jwtService: JwtService,
  ) {}

  async validateEmployee(username: string, pass: string): Promise<any> {
    const employee = await this.employeesService.findOneByUsername(username);
    const isMatch = await bcrypt.compare(pass, employee.password);
    if (employee && isMatch) {
      const { password, ...result } = employee;
      return result;
    }
    return null;
  }

  async login(req: any) {
    const payload = { username: req.user.tel, id: req.user.id };

    return {
      employee: req.user,
      access_token: this.jwtService.sign(payload),
    };
  }
}
