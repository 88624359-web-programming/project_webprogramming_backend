import { Test, TestingModule } from '@nestjs/testing';
import { MenuQueueController } from './menu-queue.controller';
import { MenuQueueService } from './menu-queue.service';

describe('MenuQueueController', () => {
  let controller: MenuQueueController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MenuQueueController],
      providers: [MenuQueueService],
    }).compile();

    controller = module.get<MenuQueueController>(MenuQueueController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
