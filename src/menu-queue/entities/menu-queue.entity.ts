import { Menu } from 'src/menus/entities/menu.entity';
import { OrderItem } from 'src/orders/entities/order-item';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class MenuQueue {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  menuStatus: string;

  @ManyToOne(() => Menu, (menu) => menu.menuQueues)
  menu: Menu;

  @ManyToOne(() => OrderItem, (orderItem) => orderItem.id)
  orderItem: OrderItem;

  @CreateDateColumn()
  createAt: Date;

  @UpdateDateColumn()
  updateAt: Date;

  @DeleteDateColumn()
  deleteAt: Date;
}
