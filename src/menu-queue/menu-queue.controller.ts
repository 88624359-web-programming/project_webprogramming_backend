import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { MenuQueueService } from './menu-queue.service';
import { CreateMenuQueueDto } from './dto/create-menu-queue.dto';
import { UpdateMenuQueueDto } from './dto/update-menu-queue.dto';

@Controller('menu-queue')
export class MenuQueueController {
  constructor(private readonly menuQueueService: MenuQueueService) {}

  @Post()
  create(@Body() createMenuQueueDto: CreateMenuQueueDto) {
    return this.menuQueueService.create(createMenuQueueDto);
  }

  @Get()
  findAll() {
    return this.menuQueueService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.menuQueueService.findOne(+id);
  }
  @Get('orderItem/:id')
  findByODItem(@Param('id') id: string) {
    return this.menuQueueService.findByODItem(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateMenuQueueDto: UpdateMenuQueueDto,
  ) {
    return this.menuQueueService.update(+id, updateMenuQueueDto);
  }
  @Patch('orderItem/:id')
  updateODItem(
    @Param('id') id: string,
    @Body() updateMenuQueueDto: UpdateMenuQueueDto,
  ) {
    return this.menuQueueService.updateODItem(+id, updateMenuQueueDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.menuQueueService.remove(+id);
  }
}
