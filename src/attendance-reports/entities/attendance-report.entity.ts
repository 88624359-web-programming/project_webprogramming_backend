import { AttendanceDetail } from 'src/attendancedetails/entities/attendancedetail.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class AttendanceReport {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'float' })
  total: number;

  @Column()
  payDate: string;

  @OneToMany(
    () => AttendanceDetail,
    (attendanceDetail) => attendanceDetail.attendanceReport,
  )
  attendanceDetails: AttendanceDetail[];

  @CreateDateColumn()
  createAt: Date;

  @UpdateDateColumn()
  updateAt: Date;

  @DeleteDateColumn()
  deleteAt: Date;
}
