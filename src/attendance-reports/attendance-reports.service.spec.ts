import { Test, TestingModule } from '@nestjs/testing';
import { AttendanceReportsService } from './attendance-reports.service';

describe('AttendanceReportsService', () => {
  let service: AttendanceReportsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AttendanceReportsService],
    }).compile();

    service = module.get<AttendanceReportsService>(AttendanceReportsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
