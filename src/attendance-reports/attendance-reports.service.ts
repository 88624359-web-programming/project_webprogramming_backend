import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateAttendanceReportDto } from './dto/create-attendance-report.dto';
import { UpdateAttendanceReportDto } from './dto/update-attendance-report.dto';
import { AttendanceReport } from './entities/attendance-report.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class AttendanceReportsService {
  constructor(
    @InjectRepository(AttendanceReport)
    private attendanceReportsRepository: Repository<AttendanceReport>,
  ) {}

  create(createAttendanceReportDto: CreateAttendanceReportDto) {
    return this.attendanceReportsRepository.save(createAttendanceReportDto);
  }

  findAll() {
    return this.attendanceReportsRepository.find({
      relations: {
        attendanceDetails: { employee: true },
      },
    });
  }

  findOne(id: number) {
    return this.attendanceReportsRepository.findOne({
      where: { id },
      relations: {
        attendanceDetails: { employee: true },
      },
    });
  }

  async findOneByDate(payDate: string) {
    return this.attendanceReportsRepository.findOne({
      where: { payDate: payDate },
      relations: {
        attendanceDetails: { employee: true },
      },
    });
  }

  async update(
    id: number,
    updateAttendanceReportDto: UpdateAttendanceReportDto,
  ) {
    const ref = await this.attendanceReportsRepository.findOneBy({ id });
    if (!ref) {
      throw new NotFoundException();
    }
    const updated = { ...ref, ...updateAttendanceReportDto };
    return this.attendanceReportsRepository.save(updated);
  }

  async remove(id: number) {
    const ref = await this.attendanceReportsRepository.findOneBy({ id });
    if (!ref) {
      throw new NotFoundException();
    }
    return this.attendanceReportsRepository.softRemove(ref);
  }
}
