import { PartialType } from '@nestjs/mapped-types';
import { CreateAttendanceReportDto } from './create-attendance-report.dto';

export class UpdateAttendanceReportDto extends PartialType(
  CreateAttendanceReportDto,
) {}
