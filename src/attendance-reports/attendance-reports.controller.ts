import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { AttendanceReportsService } from './attendance-reports.service';
import { CreateAttendanceReportDto } from './dto/create-attendance-report.dto';
import { UpdateAttendanceReportDto } from './dto/update-attendance-report.dto';
import moment from 'moment';

@Controller('attendance-reports')
export class AttendanceReportsController {
  constructor(
    private readonly attendanceReportsService: AttendanceReportsService,
  ) {}

  @Post()
  create(@Body() createAttendanceReportDto: CreateAttendanceReportDto) {
    return this.attendanceReportsService.create(createAttendanceReportDto);
  }

  @Get()
  findAll() {
    return this.attendanceReportsService.findAll();
  }

  @Get('id/:id')
  findOneById(@Param('id') id: string) {
    return this.attendanceReportsService.findOne(+id);
  }

  @Get('date/:date')
  findOneByDate(@Param('date') date: string) {
    return this.attendanceReportsService.findOneByDate(date);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateAttendanceReportDto: UpdateAttendanceReportDto,
  ) {
    return this.attendanceReportsService.update(+id, updateAttendanceReportDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.attendanceReportsService.remove(+id);
  }
}
