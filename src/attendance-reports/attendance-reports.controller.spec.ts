import { Test, TestingModule } from '@nestjs/testing';
import { AttendanceReportsController } from './attendance-reports.controller';
import { AttendanceReportsService } from './attendance-reports.service';

describe('AttendanceReportsController', () => {
  let controller: AttendanceReportsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AttendanceReportsController],
      providers: [AttendanceReportsService],
    }).compile();

    controller = module.get<AttendanceReportsController>(
      AttendanceReportsController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
