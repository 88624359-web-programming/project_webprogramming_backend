import { Module } from '@nestjs/common';
import { AttendanceReportsService } from './attendance-reports.service';
import { AttendanceReportsController } from './attendance-reports.controller';
import { AttendanceReport } from './entities/attendance-report.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([AttendanceReport])],
  controllers: [AttendanceReportsController],
  providers: [AttendanceReportsService],
})
export class AttendanceReportsModule {}
