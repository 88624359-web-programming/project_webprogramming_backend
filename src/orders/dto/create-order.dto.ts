class CreateOrderItemDto {
  menuId: number;
  quantity: number;
  status: string;
}
export class CreateOrderDto {
  storeName: string;

  tax: number;

  paymentType: string;

  cash: number;

  change: number;

  tableId: number;

  employeeId: number;

  status: string;

  orderItems: CreateOrderItemDto[];
}
