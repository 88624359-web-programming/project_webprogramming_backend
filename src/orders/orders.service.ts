import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Employee } from 'src/employees/entities/employee.entity';
import { Menu } from 'src/menus/entities/menu.entity';
import { Table } from 'src/tables/entities/table.entity';
import { Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { OrderItem } from './entities/order-item';
import { Order } from './entities/order.entity';
import { MenuQueue } from 'src/menu-queue/entities/menu-queue.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
    @InjectRepository(Menu)
    private menuRepository: Repository<Menu>,
    @InjectRepository(Table)
    private tableRepository: Repository<Table>,
    @InjectRepository(OrderItem)
    private orderItemRepository: Repository<OrderItem>,
    @InjectRepository(MenuQueue)
    private menuQueueRepository: Repository<MenuQueue>,
  ) {}

  async create(createOrderDto: CreateOrderDto) {
    const employee = await this.employeeRepository.findOneBy({
      id: createOrderDto.employeeId,
    });
    const order: Order = new Order();
    order.employee = employee;
    order.subTotal = 0;
    order.total = 0;
    order.cash = createOrderDto.cash;
    order.change = createOrderDto.change;
    order.tax = createOrderDto.tax;
    order.paymentType = createOrderDto.paymentType;
    order.storeName = createOrderDto.storeName;
    order.status = createOrderDto.status;
    order.table = await this.tableRepository.findOneBy({
      id: createOrderDto.tableId,
    });
    await this.ordersRepository.save(order); //already have id
    console.log(createOrderDto.orderItems);

    for (const od of createOrderDto.orderItems) {
      const orderItem = new OrderItem();
      orderItem.quantity = od.quantity;
      orderItem.menu = await this.menuRepository.findOneBy({
        id: od.menuId,
      });
      // console.log(orderItem.menu);

      orderItem.name = orderItem.menu.name;
      orderItem.price = orderItem.menu.price;
      orderItem.order = order;
      orderItem.total = orderItem.price * orderItem.quantity;
      orderItem.status = 'In process';
      await this.orderItemRepository.save(orderItem);
      order.subTotal = order.subTotal + orderItem.total;
      for (let i = 0; i < od.quantity; i++) {
        const menuQueue = new MenuQueue();
        menuQueue.menu = orderItem.menu;
        menuQueue.menuStatus = orderItem.status;
        menuQueue.orderItem = orderItem;
        await this.menuQueueRepository.save(menuQueue);
      }
      console.log(orderItem);
    }
    console.log(order.orderItems);
    console.log(
      'order total here',
      order.subTotal + order.subTotal * (order.tax / 100),
    );

    order.total = Math.ceil(
      order.subTotal + order.subTotal * (order.tax / 100),
    );
    await this.ordersRepository.save(order);
    // console.log(employee);
    // console.log(createOrderDto);
    return await this.ordersRepository.findOne({
      where: { id: order.id },
      relations: ['orderItems'],
    });
  }

  findAll() {
    return this.ordersRepository.find({
      //     'menu',
      //     'orderItem',
      //     'orderItem.order',
      //     'orderItem.order.table',
      relations: ['table', 'employee', 'orderItems', 'orderItems.menu'],
      // select: { table: { id: true }, employee: { id: true } },
    });
  }

  findOne(id: number) {
    return this.ordersRepository.findOne({
      where: { id: id },
      relations: {
        table: true,
        employee: true,
        orderItems: true,
      },
      // select: { table: { id: true }, employee: { id: true } },
    });
  }

  async update(id: number, updateOrderDto: UpdateOrderDto) {
    //error
    console.log('enter update');

    const order = await this.ordersRepository.findOne({
      where: { id: id },
    });
    console.log(order);

    for (const od of updateOrderDto.orderItems) {
      const orderItem = new OrderItem();
      orderItem.quantity = od.quantity;
      orderItem.menu = await this.menuRepository.findOneBy({
        id: od.menuId,
      });
      // console.log(orderItem.menu);
      orderItem.name = orderItem.menu.name;
      orderItem.price = orderItem.menu.price;
      orderItem.order = order;
      console.log('before total ODITM');
      console.log('after total ODITM');
      orderItem.total = orderItem.price * orderItem.quantity;
      orderItem.status = 'In process';
      order.subTotal = order.subTotal + orderItem.total;
      await this.orderItemRepository.save(orderItem);
      for (let i = 0; i < od.quantity; i++) {
        const menuQueue = new MenuQueue();
        menuQueue.menu = orderItem.menu;
        menuQueue.menuStatus = orderItem.status;
        menuQueue.orderItem = orderItem;
        await this.menuQueueRepository.save(menuQueue);
      }
      console.log('before total update');

      order.total = Math.ceil(
        order.subTotal + order.subTotal * (order.tax / 100),
      );
      console.log('after total update');
      console.log(orderItem);
    }
    console.log('before spread', order);

    const updateOrder = { ...order, ...updateOrderDto };
    console.log('upOrder here', updateOrder);

    return this.ordersRepository.save(order);
  }

  async updatePay(id: number, updateOrderDto: UpdateOrderDto) {
    const order = await this.ordersRepository.findOne({
      where: { id: id },
    });
    const updateOrder = { ...order, ...updateOrderDto };
    return this.ordersRepository.save(updateOrder);
  }

  async remove(id: number) {
    const order = await this.ordersRepository.findOneBy({ id: id });
    return this.ordersRepository.softRemove(order);
  }
}
