import { Menu } from 'src/menus/entities/menu.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Order } from './order.entity';
import { MenuQueue } from 'src/menu-queue/entities/menu-queue.entity';

@Entity()
export class OrderItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  quantity: number;

  @Column()
  status: string;

  @ManyToOne(() => Order, (order) => order.orderItems)
  order: Order;

  @ManyToOne(() => Menu, (menu) => menu.orderItems)
  menu: Menu;

  @OneToMany(() => MenuQueue, (menuQueue) => menuQueue.orderItem)
  menuQueues: MenuQueue[];

  @Column()
  price: number;
  @Column()
  total: number;

  @CreateDateColumn()
  createAt: Date;

  @UpdateDateColumn()
  updateAt: Date;

  @DeleteDateColumn()
  deleteAt: Date;
}
