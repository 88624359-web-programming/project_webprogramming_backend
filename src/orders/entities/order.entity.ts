import { type } from 'os';
import { Employee } from 'src/employees/entities/employee.entity';
import { Table } from 'src/tables/entities/table.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { OrderItem } from './order-item';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  dateTime: Date;

  @Column()
  storeName: string;

  @Column()
  status: string;

  @Column()
  tax: number;

  @Column()
  subTotal: number;

  @Column()
  total: number;

  @Column()
  paymentType: string;

  @Column()
  cash: number;

  @Column()
  change: number;

  @ManyToOne(() => Table, (table) => table.id)
  table: Table;

  @ManyToOne(() => Employee, (employee) => employee.orders)
  employee: Employee;

  @CreateDateColumn()
  createAt: Date;

  @UpdateDateColumn()
  updateAt: Date;

  @DeleteDateColumn()
  deleteAt: Date;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  orderItems: OrderItem[];

  // @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  // orderItems: OrderItem[];
}
