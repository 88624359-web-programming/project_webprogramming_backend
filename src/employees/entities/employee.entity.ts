import { AttendanceDetail } from 'src/attendancedetails/entities/attendancedetail.entity';
import { Order } from 'src/orders/entities/order.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 64 })
  name: string;

  @Column({ length: 64 })
  surname: string;

  @Column()
  age: number;

  @Column({ length: 10 })
  tel: string;

  @Column()
  role: string;

  @Column({ length: 20 })
  username: string;

  @Column()
  password: string;

  @OneToMany(() => Order, (order) => order.employee)
  orders: Order[];

  @CreateDateColumn()
  createAt: Date;

  @UpdateDateColumn()
  updateAt: Date;

  @DeleteDateColumn()
  deleteAt: Date;

  @OneToMany(
    () => AttendanceDetail,
    (attendanceDetail) => attendanceDetail.employee,
  )
  attendanceDetails: AttendanceDetail[];
}
