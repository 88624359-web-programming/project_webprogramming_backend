import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';
import * as bcrypt from 'bcrypt';
const saltOrRound = 10;
@Injectable()
export class EmployeesService {
  constructor(
    @InjectRepository(Employee)
    private employeesRepository: Repository<Employee>,
  ) {}

  async create(createEmployeeDto: CreateEmployeeDto) {
    const { name, surname, username } = createEmployeeDto;
    const accountExists = await this.accountExists(name, surname, username);
    if (accountExists) {
      throw new ConflictException('Employee account already exists');
    }
    const hash = await bcrypt.hash(createEmployeeDto.password, saltOrRound);
    createEmployeeDto.password = hash;
    return this.employeesRepository.save(createEmployeeDto);
  }

  findAll() {
    return this.employeesRepository.find({
      relations: { attendanceDetails: true },
    });
  }

  findOne(id: number) {
    return this.employeesRepository.findOne({
      where: { id },
      relations: { attendanceDetails: true },
    });
  }

  findOneByUsername(username: string) {
    return this.employeesRepository.findOne({
      where: { username: username },
      relations: { attendanceDetails: true },
    });
  }

  async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
    const refEmployee = await this.employeesRepository.findOneBy({ id });
    if (!refEmployee) {
      throw new NotFoundException();
    }
    if (updateEmployeeDto.password !== undefined) {
      const hash = await bcrypt.hash(updateEmployeeDto.password, saltOrRound);
      updateEmployeeDto.password = hash;
    }
    const updatedEmployee = { ...refEmployee, ...updateEmployeeDto };
    return this.employeesRepository.save(updatedEmployee);
  }

  async remove(id: number) {
    const refEmployee = await this.employeesRepository.findOneBy({ id });
    if (!refEmployee) {
      throw new NotFoundException();
    }
    return this.employeesRepository.softRemove(refEmployee);
  }

  async accountExists(name: string, surname: string, username: string) {
    const employee = await this.employeesRepository.findOne({
      where: [{ name, surname }, { username }],
    });
    return !!employee;
  }
}
