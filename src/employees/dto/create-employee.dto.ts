import { IsInt, IsNotEmpty, Length, Matches, Max, Min } from 'class-validator';

export class CreateEmployeeDto {
  @IsNotEmpty()
  @Matches(/^(?=.*[\u0E00-\u0E7F\w])[\u0E00-\u0E7F\w]{4,64}$/, {
    message:
      'Name must contain only Thai or English alphabets, with no special characters, numbers, or spaces.',
  })
  name: string;

  @IsNotEmpty()
  @Matches(/^(?=.*[\u0E00-\u0E7F\w])[\u0E00-\u0E7F\w]{4,64}$/, {
    message:
      'Surname must contain only Thai or English alphabets, with no special characters, numbers, or spaces.',
  })
  surname: string;

  @IsNotEmpty()
  @IsInt({ message: 'Age must be an integer' })
  @Min(18, { message: 'Age must be at least 18' })
  @Max(100, { message: 'Age cannot be greater than 100' })
  age: number;

  @IsNotEmpty()
  @Matches(/^(0\d{9})$/, {
    message:
      'Phone numbers must start with "0" as the first digit, followed by 9 other digits',
  })
  tel: string;

  @IsNotEmpty()
  role: string;

  @IsNotEmpty()
  @Matches(/^[a-zA-Z0-9._-]{3,20}$/, {
    message:
      'Username must be alphanumeric and between 3 and 20 characters long and consists of 3 to 20 characters and only contains letters (uppercase and lowercase), numbers, periods, underscores, or hyphens.',
  })
  username: string;

  password: string;
}
