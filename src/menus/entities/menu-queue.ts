import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Menu } from './menu.entity';

@Entity()
export class MenuQueue {
  @PrimaryGeneratedColumn()
  id: number;

  // orderItem: OrderItem;

  @Column({ length: 128 })
  name: string;

  @Column({ length: 32 })
  status: string;

  @ManyToOne(() => Menu, (menu) => menu.menuQueues)
  menu: Menu;

  @CreateDateColumn()
  createAt: Date;

  @UpdateDateColumn()
  updateAt: Date;

  @DeleteDateColumn()
  deleteAt: Date;
}
