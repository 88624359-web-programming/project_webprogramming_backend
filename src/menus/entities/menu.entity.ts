import { MenuQueue } from 'src/menu-queue/entities/menu-queue.entity';
import { OrderItem } from 'src/orders/entities/order-item';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Menu {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 128 })
  name: string;

  @Column({ length: 128, default: 'default.png' })
  imgSrc: string;

  @Column({ length: 32 })
  category: string;

  @Column({ length: 1024 })
  descript: string;

  @Column({ length: 32 })
  size: string;

  @Column({ type: 'float' })
  price: number;

  @Column({ length: 32 })
  status: string;

  @CreateDateColumn()
  createAt: Date;

  @UpdateDateColumn()
  updateAt: Date;

  @DeleteDateColumn()
  deleteAt: Date;

  @OneToMany(() => MenuQueue, (menuQueue) => menuQueue.menu)
  menuQueues: MenuQueue[];

  @OneToMany(() => OrderItem, (orderItem) => orderItem.menu)
  orderItems: OrderItem[];
}
