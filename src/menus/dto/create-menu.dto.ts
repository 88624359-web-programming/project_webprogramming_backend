import { IsInt, IsNotEmpty, Length, Matches, Min } from 'class-validator';

export class CreateMenuDto {
  @IsNotEmpty()
  @Matches(/^[\u0E00-\u0E7F -~]{3,64}$/, {
    message:
      'Menu name must be between 3 and 64 characters long and can contain Thai or English alphabet, special characters, or numbers.',
  })
  name: string;

  imgSrc = 'default.png';

  @IsNotEmpty()
  category: string;

  @IsNotEmpty()
  @Matches(/^[\u0E00-\u0E7F -~]{3,1024}$/, {
    message:
      'Menu description must be between 3 and 1024 characters long and can contain Thai or English alphabet, special characters, or numbers.',
  })
  descript: string;

  // @IsNotEmpty()
  // @Matches(/^[\u0E00-\u0E7F -~]{4,32}$/, {
  //   message:
  //     'Menu size must be between 4 and 32 characters long and can contain Thai or English alphabet, special characters, or numbers.',
  // })
  size: string;

  @IsNotEmpty()
  price: number;

  @IsNotEmpty()
  @Matches(/^[a-zA-Z]+$/, { message: 'Status must contain only letters' })
  @Matches(/^[\u0E00-\u0E7F -~]{4,32}$/, {
    message:
      'Menu status must be between 4 and 32 characters long and can contain Thai or English alphabet, special characters, or numbers.',
  })
  status: string;
}
