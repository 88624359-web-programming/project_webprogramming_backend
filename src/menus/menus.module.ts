import { Module } from '@nestjs/common';
import { MenusService } from './menus.service';
import { MenusController } from './menus.controller';
import { Menu } from './entities/menu.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MenuQueue } from 'src/menu-queue/entities/menu-queue.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Menu, MenuQueue])],
  controllers: [MenusController],
  providers: [MenusService],
})
export class MenusModule {}
