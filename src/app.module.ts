import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { type } from 'os';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EmployeesModule } from './employees/employees.module';
import { Employee } from './employees/entities/employee.entity';
import { Material } from './materials/entities/material.entity';
import { MaterialsModule } from './materials/materials.module';
import { Table } from './tables/entities/table.entity';
import { TablesModule } from './tables/tables.module';
import { OrdersModule } from './orders/orders.module';
import { MenusModule } from './menus/menus.module';
import { Menu } from './menus/entities/menu.entity';
// import { MenuQueue } from './menus/entities/menu-queue';
import { Order } from './orders/entities/order.entity';
import { OrderItem } from './orders/entities/order-item';
import { AuthModule } from './auth/auth.module';
import { AttendancedetailsModule } from './attendancedetails/attendancedetails.module';
import { AttendanceDetail } from './attendancedetails/entities/attendancedetail.entity';
import { OrderitemsModule } from './orderitems/orderitems.module';
import { MenuQueueModule } from './menu-queue/menu-queue.module';
import { MenuQueue } from './menu-queue/entities/menu-queue.entity';
import { AttendanceReportsModule } from './attendance-reports/attendance-reports.module';
import { AttendanceReport } from './attendance-reports/entities/attendance-report.entity';

@Module({
  imports: [
    EmployeesModule,
    TypeOrmModule.forRoot(
      //   {
      //   type: 'sqlite',
      //   database: 'db.sqlite',
      //   entities: [Employee, Material, Table, Menu, MenuQueue, Order, OrderItem],
      //   synchronize: true,
      // }
      {
        type: 'mysql',
        host: 'db4free.net',
        port: 3306,
        username: 'assisttech_db',
        password: 'at123456',
        database: 'assisttech_db',
        entities: [
          Employee,
          Material,
          Table,
          Menu,
          MenuQueue,
          Order,
          OrderItem,
          AttendanceDetail,
          OrderItem,
          AttendanceReport,
        ],
        synchronize: true,
      },
    ),
    MaterialsModule,
    TablesModule,
    OrdersModule,
    MenusModule,
    AuthModule,
    AttendancedetailsModule,
    OrderitemsModule,
    MenuQueueModule,
    AttendanceReportsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
